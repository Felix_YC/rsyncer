# Rsyncer

一个方便使用基于rsync同步文件的工具，更加便捷

## 安装
```
$ go install gitee.com/Felix_YC/rsyncer/cmd/rsyncer@latest
``` 
## 使用方式

config文件参考 config.demo.txt
```
$ rsyncer rsyncer.conf
``` 